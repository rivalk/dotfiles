if status is-interactive
    # Commands to run in interactive sessions can go here
end

pixterm -d 0 -s 0 -nobg /home/rival/Downloads/funky-cirno.png
set fish_greeting "			You are Funky"

set fish_function_path $fish_function_path "/usr/share/powerline/bindings/fish"
source /usr/share/powerline/bindings/fish/powerline-setup.fish
powerline-setup

alias gitbare='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'
