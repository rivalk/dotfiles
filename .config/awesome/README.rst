Package/app yg perlu diinstall
==============================
1. picom
2. nm-applet
3. thunderbird
4. Awesome Theme by Luca CPZ https://github.com/lcpz/awesome-copycats
5. AwesomeWM Widgets by streetturtle https://github.com/streetturtle/awesome-wm-widgets
6. blueberry, blueman, bluez, bluez-utils --untuk bluetooth
7. alacritty
8. nemo
9. google-chrome-stable, brave
10. nitrogen
11. light,xorg-xbacklight, brighnessctl --buat pengaturan brightness layar

Tampilan

.. image:: https://i.imgur.com/kjxzhbA.png
